<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link href="/css/styles.css" rel="stylesheet">
</head>
<body>
    <div>
        <table>
            <thead>
                <tr>
                    <th id="user_id_th">User Id</th>
                    <th id="user_name_th">Name</th>
                    <th id="user_email_th">Email</th>
                    <th id="user_actions_th">Actions</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${users}" var="user">
                    <tr>
                        <td>${user.id}</td>
                        <td>${user.name}</td>
                        <td>${user.email}</td>
                        <td>
                            <form method="POST" action="<c:out value="/users/${user.id}/delete"/>">
                                <input type="submit" name="delete-comment" value="Delete">
                            </form>
                            <form method="GET" action="<c:out value="/users/${user.id}"/>">
                                <input type="submit" value="See and edit info">
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <hr align="left" width="100%">
        <table>
            <tr>
                <form method="POST" action="<c:out value="/users"/>">
                    <td width="60px" id="user_name_td"><input type="text" name="name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Name ...'" placeholder="Name ..."/></td>
                    <td width="60px" id="user_email_td"><input type="text" name="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email ...'" placeholder="Email ..."/></td>
                    <td id="user_actions_td"><input type="submit" value="Add"></td>
                </form>
        </table>
        <hr align="left" width="100%">
        <form>
            <input type="hidden" name="order" value="asc" />
            <button type="submit">Sort by name ascending</button>
        </form>
        <form>
            <input type="hidden" name="order" value="desc" />
            <button type="submit">Sort by id descending</button>
        </form>
        <form action="/users">
            <button type="submit">Default order</button>
        </form>
    </div>
</body>
</html>

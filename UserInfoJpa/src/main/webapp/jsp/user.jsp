<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Title</title>
        <link href="/css/styles.css" rel="stylesheet">
    </head>
    <body>
        <table>
            <form method="POST" action='<c:out value="/users/${user.get().id}"/>'>
            <tr><td>ID: </td><td><c:out value="${user.get().id}"/></td></tr>
            <tr><td>Name: </td><td><input type="text" name="name" value='<c:out value="${user.get().name}"/>'/></td></tr>
            <tr><td>Email: </td><td><input type="text" name="email" value='<c:out value="${user.get().email}"/>'/></td></tr>

            <c:if test="${empty user.get().phoneNumbers}">
                <tr><td>Phone 1: </td><td><input type="text" name="phone"/></td></tr>
            </c:if>
            <c:forEach items="${user.get().phoneNumbers}" var="number" varStatus="counter">
                    <tr><td>Phone ${counter.count}: </td><td><input type="text" name="phone" value='<c:out value="${number}"/>'/></td></tr>
            </c:forEach>

            <c:if test="${empty user.get().addresses}">
                <tr><td>Address Line1: </td><td><input type="text" name="addressLine1" /></td></tr>
                <tr><td>Address Line2: </td><td><input type="text" name="addressLine2" /></td></tr>
                <tr><td>City: </td><td><input type="text" name="city" /></td></tr>
                <tr><td>State: </td><td><input type="text" name="state" /></td></tr>
                <tr><td>Country: </td><td><input type="text" name="country" /></td></tr>
                <tr><td>Zip Code:</td><td><input type="text" name="zipcode" /></td></tr>
            </c:if>

            <c:forEach items="${user.get().addresses}" var="address">
                <tr><td>Address Line1: </td><td><input type="text" name="addressLine1" value='<c:out value="${address.addressLine1}"/>'/></td></tr>
                <tr><td>Address Line2: </td><td><input type="text" name="addressLine2" value='<c:out value="${address.addressLine2}"/>'/></td></tr>
                <tr><td>City: </td><td><input type="text" name="city" value='<c:out value="${address.city}"/>'/></td></tr>
                <tr><td>State: </td><td><input type="text" name="state" value='<c:out value="${address.state}"/>'/></td></tr>
                <tr><td>Country: </td><td><input type="text" name="country" value='<c:out value="${address.country}"/>'/></td></tr>
                <tr><td>Zip Code: </td><td><input type="text" name="zipcode" value='<c:out value="${address.zipCode}"/>'/></td></tr>
            </c:forEach>
            <tr><td colspan="2" id="user_actions_td"><input style="width: 100%; color: red" type="submit" value="Save"></td></tr>
            </form>
            <form method="get" action="/users">
                <tr><td colspan="2" ><button style="width: 100%; color: red">Back to users page</button></td></tr>
            </form>
        </table>
        <p style="color: blue; font-size: 10px;">* Blue fields are editable</p>
    </body>
</html>

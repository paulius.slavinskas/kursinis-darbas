<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Blog!</title>
        <link href="/css/style.css" rel="stylesheet">
    </head>
    <body>
        <div>
            <h3 class="hello-title">Welcome JPA example app!</h3>
            <p>You can see all users by clicking <a href="/users">here</a></p>
            <div>
                <script src="/js/script.js"></script>
            </div>
        </div>
    </body>
</html>
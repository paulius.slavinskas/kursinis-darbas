package com.example.jpa.utils;

import java.util.List;

public class BubbleSorter {
    public <T extends Comparable> void sort(List<T> coll){
        boolean swapped = true;
        int n = coll.size();
        for (int i = 0; i < n - 1 & swapped; i++){
            swapped = false;
            for (int j = 0; j < n - i - 1; j++){
                if (coll.get(j).compareTo(coll.get(j + 1)) > 0){
                    T temp = coll.get(j);
                    coll.set(j, coll.get(j + 1));
                    coll.set(j + 1, temp);
                    swapped = true;
                }
            }
        }
    }

    public <T extends Comparable> void sortReverse(List<T> coll){
        boolean swapped = true;
        int n = coll.size();
        for (int i = 0; i < n - 1 & swapped; i++){
            swapped = false;
            for (int j = 0; j < n - i - 1; j++){
                if (coll.get(j).compareTo(coll.get(j + 1)) < 0){
                    T temp = coll.get(j);
                    coll.set(j, coll.get(j + 1));
                    coll.set(j + 1, temp);
                    swapped = true;
                }
            }
        }
    }
}

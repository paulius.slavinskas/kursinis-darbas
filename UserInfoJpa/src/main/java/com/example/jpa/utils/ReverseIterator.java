package com.example.jpa.utils;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class ReverseIterator implements Iterator {

    private List originalList;
    private int currentPosition;

    public ReverseIterator(List originalList) {
        this.originalList = originalList;
        currentPosition = originalList.size() - 1;
    }

    @Override
    public boolean hasNext() {
        return currentPosition > -1;
    }

    @Override
    public Object next() {
        if(hasNext()) return originalList.get(currentPosition--);
        else throw new NoSuchElementException();
    }
}


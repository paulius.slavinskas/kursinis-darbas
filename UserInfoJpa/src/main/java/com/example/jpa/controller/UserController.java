package com.example.jpa.controller;

import com.example.jpa.service.UserServiceCachingProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class UserController {

    @Autowired
    private UserServiceCachingProxy userService;

    public UserController(UserServiceCachingProxy userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    public String listUsers(Model model, @RequestParam(required = false) String order) {
        if(order != null && order.equals("asc")){
            model.addAttribute("users", userService.getAllSorted());
            return "users";
        } else if(order != null && order.equals("desc")) {
            model.addAttribute("users", userService.getAllSortedReverse());
            return "users";
        } else {
            model.addAttribute("users", userService.getAllUsers());
            return "users";
        }
    }

    @GetMapping("/users/{id}")
    public String getUserInfo(Model model, @PathVariable Long id) {
        model.addAttribute("user", userService.getUserById(id));
        return "user";
    }

    @PostMapping("/users")
    public ModelAndView addUser(ModelMap model, @RequestParam Map<String, String> body){
        userService.saveUser(body.get("name"), body.get("email"));
        model.addAttribute("users", userService.getAllUsers());
        return new ModelAndView("redirect:/users", model);
    }

    @PostMapping("/users/{id}")
    public ModelAndView editUser(ModelMap model,
         @PathVariable Long id,
         @RequestParam Map<String, String> body
    ){
        userService.editUser(id, body);
        model.addAttribute("user", userService.getUserById(id));
        return new ModelAndView("redirect:/users/" + id, model);
    }

    @PostMapping("/users/{id}/delete")
    public ModelAndView deleteUser(ModelMap model, @PathVariable Long id){
        userService.deleteUserById(id);
        model.addAttribute("users", userService.getAllUsers());
        return new ModelAndView("redirect:/users", model);
    }
}

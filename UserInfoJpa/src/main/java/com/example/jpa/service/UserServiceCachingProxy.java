package com.example.jpa.service;

import com.example.jpa.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class UserServiceCachingProxy {

    @Autowired
    private UserService userService;
    private boolean cacheDirty = true;
    private List<User> unsortedUsers;
    private List<User> sortedUsers;
    private List<User> reverseSortedUsers;

    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    public List<User> getAllSorted(){ return userService.getAllSorted(); }

    public List<User> getAllSortedReverse(){ return userService.getAllSortedReverse(); }

    // ... simple in memory caching mechanism, not implemented fully
    public Optional<User> getUserById(long id) {
        if(!cacheDirty) return Optional.of(getByIdFromCache(id));
        return userService.getUserById(id);
    }

    public void saveUser(String name, String email) {
        userService.saveUser(name, email);
        cacheDirty = true;
        // ... run async cache update
    }

    public void editUser(Long id, Map<String, String> body) {
        userService.editUser(id, body);
        cacheDirty = true;
        // ... run async cache update
    }

    public void deleteUserById(Long id) {
        userService.deleteUserById(id);
        cacheDirty = true;
        // ... run async cache update
    }

    // ... helper methods
    private User getByIdFromCache(long id){
        // ... implement binary search on sorted collection
        User user = null;
        for (User u : unsortedUsers)
            if(u.getId() == id) return u;
        return user;
    }

    private void renewCache(){
        this.unsortedUsers = userService.getAllUsers();
        this.sortedUsers = userService.getAllSorted();
        this.reverseSortedUsers = userService.getAllSortedReverse();
    }
}

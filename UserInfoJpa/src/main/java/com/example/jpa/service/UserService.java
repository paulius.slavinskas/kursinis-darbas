package com.example.jpa.service;

import com.example.jpa.model.Address;
import com.example.jpa.model.User;
import com.example.jpa.repository.UserRepository;
import com.example.jpa.utils.BubbleSorter;
import com.example.jpa.utils.ReverseIterator;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

public class UserService {

    @Autowired
    private UserRepository userRepository;
    private BubbleSorter sorter;

    public UserService(BubbleSorter bubbleSorter) {
        this.sorter = bubbleSorter;
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public List<User> getAllSorted(){
        List<User> users = userRepository.findAll();
        sorter.sort(users);
        return users;
    }

    public List<User> getAllSortedReverse(){
        List<User> users = userRepository.findAll();
        // sorter.sortReverse(users);
        ReverseIterator ri = new ReverseIterator(users);
        List<User> usersReversed = new ArrayList<>();
        while(ri.hasNext())
            usersReversed.add((User) ri.next());

        return usersReversed;
    }

    public Optional<User> getUserById(long id) {
        return userRepository.findById(id);
    }

    public void saveUser(String name, String email) {
        User user = new User.Builder(name, email).build();
        userRepository.save(user);
    }

    public void editUser(Long id, Map<String, String> body) {
        String nName = body.get("name");
        String nEmail = body.get("email");
        Set<String> nPhones = new HashSet<>(){{ add(body.get("phone")); }};

        String nAddressLine1 = body.get("addressLine1");
        String nAddressLine2 = body.get("addressLine2");
        String nCity = body.get("city");
        String nState = body.get("state");
        String nCountry = body.get("country");
        String nZipcode = body.get("zipcode");
        Address nAddress = new Address(nAddressLine1, nAddressLine2, nCity, nState, nCountry,  nZipcode);
        Set<Address> nAddresses =  new HashSet<>() {{ add(nAddress); }};

        User user = getUserById(id).get();
        user.setName(nName);
        user.setEmail(nEmail);
        user.setPhoneNumbers(nPhones);
        user.setAddresses(nAddresses);

        userRepository.save(user);
    }

    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }
}

# DDL
ALTER TABLE `user_profiles` DROP `city`, DROP `country`, DROP `phone_number`, DROP `state`, DROP `street`, DROP `zip_code`;

# DML
# Inserting data for old user records
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('1', '15', 'Kauno', 'Kaunas', 'Lithuania', 'N/A', '61551');
UPDATE `user_profiles` SET `dob` = '1988-12-01', `gender` = 'MALE', `user_id` = '1' WHERE `id` = '1';
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('1', '+37062922551');

INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('2', '329', 'Juozapavičiaus', 'Vilnius', 'Lithuania', 'N/A', '61552');
UPDATE `user_profiles` SET `dob` = '1988-12-02', `gender` = 'MALE', `user_id` = '2' WHERE `id` = '2';
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('2', '+37062922552');

# Inserting new data
INSERT INTO `users` (`email`, `name`) VALUES ('antanas@gmail.com', 'Antanas');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('3', '32', 'Stoties', 'Vilnius', 'Lithuania', 'N/A', '61558');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('3', '1988-12-03', 'MALE', '3');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('3', '+37062922553');

INSERT INTO `users` (`email`, `name`) VALUES ('pranas@gmail.com', 'Pranas');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('4', '321', 'Basanavičiaus', 'Klaipėda', 'Lithuania', 'N/A', '71552');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('4', '1988-12-04', 'MALE', '4');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('4', '+37062922554');

INSERT INTO `users` (`email`, `name`) VALUES ('marija@gmail.com', 'Marija');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('5', '331', 'Elektros', 'Mažeikiai', 'Lithuania', 'N/A', '51552');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('5', '1988-12-05', 'FEMALE', '5');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('5', '+37062922555');

INSERT INTO `users` (`email`, `name`) VALUES ('roberta@gmail.com', 'Roberta');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('6', '335', 'Islandijos', 'Jurbarkas', 'Lithuania', 'N/A', '21552');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('6', '1988-12-06', 'FEMALE', '6');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('6', '+37062922556');

INSERT INTO `users` (`email`, `name`) VALUES ('agne@gmail.com', 'Agnė');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('7', '12', 'Perkėlos', 'Linkuva', 'Lithuania', 'N/A', '22552');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('7', '1988-12-07', 'FEMALE', '7');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('7', '+37062922557');

INSERT INTO `users` (`email`, `name`) VALUES ('juozas@gmail.com', 'Juozas');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('8', '199', 'Traukinių', 'Šalčininkai', 'Lithuania', 'N/A', '62552');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('8', '1988-12-08', 'MALE', '8');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('8', '+37062922558');

INSERT INTO `users` (`email`, `name`) VALUES ('mindaugas@gmail.com', 'Mindaugas');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('9', '1', 'Kalnakasių', 'Šalčininkai', 'Lithuania', 'N/A', '62550');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('9', '1988-12-09', 'MALE', '9');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('9', '+37062922559');

INSERT INTO `users` (`email`, `name`) VALUES ('gintautas@gmail.com', 'Gintautas');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('10', '112', 'Akmenų', 'Vilnius', 'Lithuania', 'N/A', '61114');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('10', '1988-12-10', 'MALE', '10');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('10', '+37062922510');

INSERT INTO `users` (`email`, `name`) VALUES ('sartis@gmail.com', 'Sartis');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('11', '163', 'Žirgų', 'Kaunas', 'Lithuania', 'N/A', '66114');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('11', '1988-12-11', 'MALE', '11');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('11', '+37062922511');

INSERT INTO `users` (`email`, `name`) VALUES ('aleksas@gmail.com', 'Aleksas');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('12', '8', 'Krakmolo', 'Vilnius', 'Lithuania', 'N/A', '62119');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('12', '1988-12-12', 'MALE', '12');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('12', '+37062922512');

INSERT INTO `users` (`email`, `name`) VALUES ('dovilė@gmail.com', 'Dovilė');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('13', '8', 'Traikos', 'Jurbarkas', 'Lithuania', 'N/A', '51119');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('13', '1988-12-13', 'MALE', '13');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('13', '+37062922513');

INSERT INTO `users` (`email`, `name`) VALUES ('sandra@gmail.com', 'Sandra');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('14', '91', 'Krėvės', 'Kaunas', 'Lithuania', 'N/A', '61111');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('14', '1988-12-14', 'MALE', '14');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('14', '+37062922514');

INSERT INTO `users` (`email`, `name`) VALUES ('samanta@gmail.com', 'Samanta');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('15', '11', 'Jurgiškių', 'Alytus', 'Lithuania', 'N/A', '12111');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('15', '1988-12-15', 'MALE', '15');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('15', '+37062922516');

INSERT INTO `users` (`email`, `name`) VALUES ('marijus@gmail.com', 'Marijus');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('16', '56', 'Sąvartyno', 'Alytus', 'Lithuania', 'N/A', '12131');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('16', '1988-12-16', 'MALE', '16');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('16', '+37062922517');

INSERT INTO `users` (`email`, `name`) VALUES ('arūnas@gmail.com', 'Arūnas');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('17', '66', 'Gulbių', 'Klaipėda', 'Lithuania', 'N/A', '52131');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('17', '1988-12-17', 'MALE', '17');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('17', '+37062922518');

INSERT INTO `users` (`email`, `name`) VALUES ('inga@gmail.com', 'Inga');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('18', '67', 'Raganų', 'Kupiškis', 'Lithuania', 'N/A', '52171');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('18', '1988-12-18', 'MALE', '18');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('18', '+37062922518');

INSERT INTO `users` (`email`, `name`) VALUES ('valdas@gmail.com', 'Valdas');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('19', '68', 'Šluotų', 'Visaginas', 'Lithuania', 'N/A', '52173');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('19', '1988-12-19', 'MALE', '19');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('19', '+37062922519');

INSERT INTO `users` (`email`, `name`) VALUES ('džeimsas@gmail.com', 'Džeimsas');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('20', '69', 'Baldų', 'Utena', 'Lithuania', 'N/A', '52174');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('20', '1988-12-20', 'MALE', '20');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('20', '+37062922520');

INSERT INTO `users` (`email`, `name`) VALUES ('elis@gmail.com', 'Elis');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('21', '70', 'Baseino', 'Utena', 'Lithuania', 'N/A', '52175');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('21', '1988-12-21', 'MALE', '21');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('21', '+37062922521');

INSERT INTO `users` (`email`, `name`) VALUES ('mikas@gmail.com', 'Mikas');
INSERT INTO `user_addresses`
    (`user_id`, `house_number`, `street`, `city`, `country`, `state`, `zip_code`) VALUES
    ('22', '71', 'Meninikų', 'Utena', 'Lithuania', 'N/A', '52176');
INSERT INTO `user_profiles`
    (`id`, `dob`, `gender`, `user_id`) VALUES
    ('22', '1988-12-22', 'MALE', '22');
INSERT INTO `user_phone_numbers`
    (`user_id`, `phone_number`) VALUES
    ('22', '+37062922522');
# DDL
ALTER DATABASE `userinfocrud` CHARACTER SET utf8 COLLATE utf8_lithuanian_ci;

CREATE TABLE `users` (
     `id` bigint(20) NOT NULL AUTO_INCREMENT,
     `email` varchar(100) NOT NULL,
     `name` varchar(100) NOT NULL,
     PRIMARY KEY (`id`),
     UNIQUE KEY `UK_6dotkott2kjsp8vw4d0m25fb7` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

CREATE TABLE `user_addresses` (
      `user_id` bigint(20) NOT NULL,
      `house_number` varchar(255) DEFAULT NULL,
      `street` varchar(255) DEFAULT NULL,
      `city` varchar(255) DEFAULT NULL,
      `country` varchar(255) DEFAULT NULL,
      `state` varchar(255) DEFAULT NULL,
      `zip_code` varchar(255) DEFAULT NULL,
      KEY `FKn2fisxyyu3l9wlch3ve2nocgp` (`user_id`),
      CONSTRAINT `FKn2fisxyyu3l9wlch3ve2nocgp` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

CREATE TABLE `user_profiles` (
     `id` bigint(20) NOT NULL AUTO_INCREMENT,
     `address1` varchar(100) DEFAULT NULL,
     `address2` varchar(100) DEFAULT NULL,
     `city` varchar(100) DEFAULT NULL,
     `country` varchar(100) DEFAULT NULL,
     `dob` date DEFAULT NULL,
     `gender` varchar(10) DEFAULT NULL,
     `phone_number` varchar(15) DEFAULT NULL,
     `state` varchar(100) DEFAULT NULL,
     `street` varchar(100) DEFAULT NULL,
     `zip_code` varchar(32) DEFAULT NULL,
     `user_id` bigint(20) NOT NULL,
     PRIMARY KEY (`id`),
     UNIQUE KEY `UK_e5h89rk3ijvdmaiig4srogdc6` (`user_id`),
     CONSTRAINT `FKjcad5nfve11khsnpwj1mv8frj` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

CREATE TABLE `user_phone_numbers` (
      `user_id` bigint(20) NOT NULL,
      `phone_number` varchar(255) DEFAULT NULL,
      KEY `FKeut9prmcgpertwb3vr2ik80bq` (`user_id`),
      CONSTRAINT `FKeut9prmcgpertwb3vr2ik80bq` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

# DML
INSERT INTO `users` (`email`, `name`) VALUES ('jonas@gmail.com', 'Jonas');
INSERT INTO `users` (`email`, `name`) VALUES ('petras@gmail.com', 'Petras');

INSERT INTO `user_profiles`
    (`address1`, `address2`, `city`, `country`, `dob`, `gender`, `phone_number`, `state`, `street`, `zip_code`, `user_id`) VALUES
    ('15', 'N/A', 'Kaunas', 'Lithuania', '1988-06-10', 'MALE', '+37062955522', 'N/A', 'Kauno', '61551', '1');
INSERT INTO `user_profiles`
    (`address1`, `address2`, `city`, `country`, `dob`, `gender`, `phone_number`, `state`, `street`, `zip_code`, `user_id`) VALUES
    ('329', 'N/A', 'Vilnius', 'Lithuania', '1965-07-18', 'MALE', '+37062955333', 'N/A', 'Juozapavičiaus', '61552', '2');
